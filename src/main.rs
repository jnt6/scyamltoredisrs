use redis::Commands;
use serde::{Deserialize, Serialize};
use serde_yaml::{self};
use std::env;
// use serde_yaml::{yaml, Error, Value};

#[derive(Debug, Deserialize, Serialize)]
struct Suitcase {
    files: Vec<FileEntry>,
}

#[derive(Debug, Deserialize, Serialize)]
struct FileEntry {
    path: String,
    destination: String,
    name: String,
    size: i64,
    suitcase_index: i64,
    suitcase_name: String,
}

fn main() {
    println!("Hello, world!");
    let f = std::fs::File::open("tmp/inventory.yaml")
        .expect("Could not open file.");

    let mut conn = connect_redis();
    verify_redis(&mut conn);
    let parsed: Suitcase = serde_yaml::from_reader(f).expect("parse failed!");
    println!("{:?}", parsed.files.len());
    for el in parsed.files {
        // println!("{:?}", el);
        let _: () = conn
            .hset_multiple(
                format!("{}:{}", env::var("REDIS_KEY").unwrap(), el.path),
                &[
                    ("path", el.path),
                    ("name", el.name),
                    ("size", el.size.to_string()),
                    ("suitcase_index", el.suitcase_index.to_string()),
                    ("destination", el.destination),
                    ("suitcase_name", el.suitcase_name),
                ],
            )
            .expect("failed to execute HSET");
    }
}

fn verify_redis(conn: &mut redis::Connection) {
    let _: () = redis::cmd("SET")
        .arg("foo")
        .arg("bar")
        .query(conn)
        .expect("failed to execute SET for 'foo'");
    let bar: String = redis::cmd("GET")
        .arg("foo")
        .query(conn)
        .expect("failed to execute GET for 'foo'");
    println!("value for 'foo' = {}", bar);
}

fn connect_redis() -> redis::Connection {
    //format - host:port
    let redis_host_name =
        env::var("REDIS_HOSTNAME").expect("missing environment variable REDIS_HOSTNAME");

    let redis_password = env::var("REDIS_PASSWORD").unwrap_or_default(); //if Redis server needs secure connection
    let uri_scheme = match env::var("IS_TLS") {
        Ok(_) => "rediss",
        Err(_) => "redis",
    };
    let redis_conn_url = format!("{}://:{}@{}", uri_scheme, redis_password, redis_host_name);
    redis::Client::open(redis_conn_url)
        .expect("Invalid connection URL")
        .get_connection()
        .expect("failed to connect to Redis")
}
